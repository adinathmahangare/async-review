const fs = require('fs').promises;
const output = require('./index.json');
const indexFilePath = './index.json';

async function getContent(indexFilePath) {

        const data = await fs.readFile(indexFilePath, 'utf-8');       
        const mainData = JSON.parse(data);

        const topicDirectories = mainData.topics.map((topic) => {
            return topic.directory_path;
        });
        
        for (let topicDirectory of topicDirectories) {
            const data = await fs.readFile(`./${topicDirectory}/index.json`, 'utf-8')
            const topicData = JSON.parse(data);

            const contentLocations = topicData.parts.map((contentFile) => {
                return contentFile.file_location;
            });

            let result = [];
            for (let contentLocation of contentLocations) {
                const data = await fs.readFile(`./${contentLocation}`, 'utf-8');

                const contentArray = data.split("\n");
                const partsObject = {};
                partsObject['name'] = contentArray[0];
                partsObject['content'] = contentArray[2];
                result.push(partsObject);
                
            }
            
            output.topics.forEach((element, index, arrayOutput)=>{
                if(element['directory_path'] === topicDirectory){
                    arrayOutput[index]['name'] = topicData['name'];
                    arrayOutput[index]['parts'] = result;
                }
            })
        }
        const answer = JSON.stringify(output , null ,4);
        const dataPromise = await fs.writeFile('./output.json' , answer , {encoding:'utf8'} );
}

getContent(indexFilePath);